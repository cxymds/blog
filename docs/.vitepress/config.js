// 头部导航
import topNav from "./topNav";
import { vitePressNote } from "./sideBar/vitePressBar";
// import { terser } from "vite-plugin-terser";

export default {
  title: "前端吧",
  description: "关注web前端开发为主的博客网站和前端网址大全",
  // 打包目录
  dest: "./dist",
  head: [
    // 添加图标
    ["link", { rel: "icon", href: "/favicon.ico" }],
  ],
  locales: {
    zh: {
      label: "简体中文",
      lang: "zh",
    },
    en: {
      label: "English",
      lang: "en",
      link: "/en/foo", // 跳转到英文文档页面，当然也可以是外部的
    },
  },
  // 主题配置
  themeConfig: {
    // 导航上的logo
    logo: "/logo.svg",
    // 隐藏logo右边的标题
    siteTitle: false,
    // 头部导航栏配置
    nav: topNav,
    // 左侧导航栏
    sidebar: {
      "/note/vitePress": vitePressNote,
    },
    // 站点页脚配置
    footer: {
      message: "Released under the MIT License",
      copyright: "Copyright © 2023-present Lao Yuan",
    },
    socialLinks: [
      { icon: "github", link: "https://github.com/vuejs/vitepress" },
      // 也可以自定义svg的icon:
      {
        icon: {
          svg: '<svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>Dribbble</title><path d="M12...6.38z"/></svg>',
        },
        link: "...",
      },
    ],

    // 右侧边栏配置，默认值是"In hac pagina"
    outlineTitle: "本页目录",
    lastUpdatedText: "最后更新", // string
  },
  // 获取每个文件最后一次 git 提交的 UNIX 时间戳(ms)，同时它将以合适的日期格式显示在每一页的底部
  lastUpdated: true, // string | boolean
  // 搜索
  algolia: {
    apiKey: "your_api_key",
    indexName: "index_name",
  },
  vite: {
    build: {
      minify: false,
    },
  },
};
