vue

## 本地构建与测试[](https://vitepress.dev/zh/guide/deploy#build-and-test-locally)

1. 可以运行以下命令来构建文档：

   sh

   ```
   $ npm run docs:build
   ```

2. 构建文档后，通过运行以下命令可以在本地预览它：

   sh

   ```
   $ npm run docs:preview
   ```

   `preview` 命令将启动一个本地静态 Web 服务 `http://localhost:4173`，该服务以 `.vitepress/dist` 作为源文件。这是检查生产版本在本地环境中是否正常的一种简单方法。

3. 可以通过传递 `--port` 作为参数来配置服务器的端口。

   json

   ```
   {
     "scripts": {
       "docs:preview": "vitepress preview docs --port 8080"
     }
   }
   ```

   现在 `docs:preview` 方法将会在 `http://localhost:8080` 启动服务。

## 设定 public 根目录[](https://vitepress.dev/zh/guide/deploy#setting-a-public-base-path)

默认情况下，我们假设站点将部署在域名 (`/`) 的根路径上。如果站点在子路径中提供服务，例如 `https://mywebsite.com/blog/`，则需要在 VitePress 配置中将 [`base`](https://vitepress.dev/zh/reference/site-config#base) 选项设置为 `'/blog/'`。

**例**：如果你使用的是 Github（或 GitLab）页面并部署到 `user.github.io/repo/`，请将 `base` 设置为 `/repo/`。
